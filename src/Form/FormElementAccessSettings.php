<?php

namespace Drupal\form_element_access\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ElementAccessForm.
 *
 * @package Drupal\element_access\Form
 */
class FormElementAccessSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'form_element_access.config',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'form_element_access_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('form_element_access.config');

    $form['enable_element_tags'] = array(
      '#type' => 'checkbox',
      '#title' => t('Show form element tags in element description.'),
      '#default_value' => $config->get('enable_element_tags'),
    );

    // get roles in system without anonymous
    $roles = array_keys(user_roles(TRUE));

    foreach ($roles as $role) {
      $form[$role . '_config'] = array(
        '#type' => 'textarea',
        '#title' => t('Role to exclude elements from: ') . $role,
        '#default_value' => $config->get($role . '_config'),
        '#description' => t('Please enter element tokens to exclude form elements, one by line. To force inclusion, prefix with "!", e.g. !node_article_form::body')
      );
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $values = $form_state->getValues();

    foreach ($values as $key => $value) {
      if (preg_match('/_config/', $key)) {
        $set_error = FALSE;

        // get line by line in the textarea
        $items = explode(PHP_EOL, $value);
        foreach ($items as $item) {
          if (!empty($item) && !preg_match('/::/', $item)) {
            $set_error = TRUE;
          }
        }
        // set error for the element we found an issue
        if ($set_error) {
          $form_state->setErrorByName($key, t('Format of element is wrong. Please correct like: my_form::my_element'));
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Fetch configuration to write into
    $config = $this->config('form_element_access.config');

    // Fetch all entered values
    $values = $form_state->getValues();

    // get roles in system without anonymous
    // todo: might be better to split rows and save as array
    $roles = array_keys(user_roles(TRUE));
    foreach ($roles as $role) {
      $config->set($role . '_config', $values[$role . '_config']);
    }

    $config->set('enable_element_tags', $values['enable_element_tags']);

    $config->save();
  }

}
