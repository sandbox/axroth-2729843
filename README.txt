---SUMMARY---

Form Element Access can be used to hide form elements depending on user roles

For a full description visit project page:
https://www.drupal.org/project/form_element_access

Bug reports, feature suggestions and latest developments:
http://drupal.org/project/issues/form_element_access


---REQUIREMENTS---


*None. (Other than a clean Drupal 8 installation)


---INSTALLATION---


Install as usual.

---CONFIGURATION---

*Visit the module settings page under /admin/config/form_element_access. There you'll find an option to display form element tags in forms, and several text areas based on user-roles to list form elements (using their tags) to be excluded for the corresponding user role. It's possible to override an exclusion using a '!' prefix (e.g. exclude for authenticated users, but not for administrators).
*There's a permission to edit the settings form, too.

For help regarding installation, visit:
https://www.drupal.org/documentation/install/modules-themes/modules-8


---CONTACT---


Current Maintainers:
*Axel Roth (axroth) - https://www.drupal.org/u/axroth


This project has been sponsored by:

*arocom

arocom is a leading Drupal agency located in Stuttgart, Germany
Visit: http://www.arocom.de for more information.
